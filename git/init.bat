@echo off

setlocal

set dotfiles=%USERPROFILE%\Dotfiles
set dotgit=%USERPROFILE%\Dotfiles\git

rem Make gitconfig.
set config=%dotgit%\gitconfig
set tmp=%dotgit%\gitconfig_template_win
set copied=%USERPROFILE%\.gitconfig
copy %tmp% %copied%

rem Make gitconfig and symbolic link of tigrc.
set rc=%dotgit%\tigrc
set target=%USERPROFILE%\.tigrc
powershell.exe -Command Start-Process ^
               -FilePath "cmd" ^
               -ArgumentList "/c", "mklink", "%rc%", "%target%" ^
               -Verb Runas

rem Edit gitconfig
notepad.exe %copied%
