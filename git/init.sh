#!/bin/sh
HERE=$(cd $(dirname $0); pwd)

ln -sf $HERE/gitconfig $HOME/.gitconfig
ln -sf $HERE/tigrc     $HOME/.tigrc
