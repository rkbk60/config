#!/bin/bash
export PYENV_ROOT="$HOME/.config/dotfiles/.data/pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init - | psub)"
eval "$(pyenv virtualenv-init - | psub)"
nvim-qt --no-ext-tabline
