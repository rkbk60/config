#!/bin/sh
HERE=$(cd $(dirname $0); pwd)

ln -sf $HERE/vimrc  ~/.vimrc
ln -sf $HERE/gvimrc ~/.gvimrc

if type nvim 1>/dev/null 2>&1; then
    TARGET=$HOME/.config/nvim
    [ -d $TARGET ] || mkdir -p $TARGET
    ln -sf $HERE/vimrc  $TARGET/init.vim
    ln -sf $HERE/gvimrc $TARGET/ginit.vim
fi
