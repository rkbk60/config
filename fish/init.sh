#!/bin/sh
HERE=$(cd $(dirname $0); pwd)
TARGET=$HOME/.config/fish

[ -d $TARGET ] || mkdir -P $TARGET
ln -sf $HERE/config.fish $TARGET/config.fish
type fish 1>/dev/null 2>&1 && fish $HERE/fish/config.fish
