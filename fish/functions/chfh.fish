function chfh
    switch "$argv"
        case "start" "s":
            set -gx __SHELL_CACHE $SHELL
            set -gx SHELL fish
        case "end" "e" "finish" "f":
            test -qg __SHELL_CACHE
                and set SHELL $__SHELL_CACHE
        end
end
