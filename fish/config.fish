set fish_greeting
alias is_succeeded 'test $status -eq 0'
alias is_wsl "uname -a | grep -lq 'Microsoft'"
alias is_termux "uname -a | grep -lq 'Android'"
alias is_arch "uname -a | grep -lq 'ARCH'"
alias config '$EDITOR $FISH_CONFIG_PATH'

# set variables {{{1
set -g FISH_CONFIG_PATH (realpath (status filename))
set -x EDITOR vim
set -x VISUAL vim
set -q XDG_CONFIG_HOME
    or set XDG_CONFIG_HOME $HOME/.config
set -x PIPENV_VENV_IN_PROJECT 'true'
#}}}1

# update functions {{{1
for fn_type in functions completions
    test -d $HOME/.config/fish/$fn_type/
        or mkdir -p $HOME/.config/fish/$fn_type
    set -l fn_from (string replace '/config.fish' "/$fn_type" $FISH_CONFIG_PATH)
    set -l fn_to   "$HOME/.config/fish/$fn_type"
    find $fn_from/*.fish -maxdepth 0 \
        | string replace -ar \
            '^.*/([^/]*)\.fish.*$' \
            'ln -sf @1$1.fish @2$1.fish ^/dev/null >&2 &' \
        | string replace -a  '@1' $fn_from/ \
        | string replace -a  '@2' $fn_to/ \
        | source
    # remove blocken links
    find -L $fn_to/ -type l \
        | string replace -r \
            '^(.*\.fish)?' \
            'rm -f $1 ^/dev/null >&1 &' \
        | source
end
#}}}1

# update history {{{1
echo "all" | history delete -c "exit" >/dev/null ^&1
#}}}1

# install fisher and plugins automatically {{{1
ln -sf (__dotfiles_path)/fish/fishfile $XDG_CONFIG_HOME/fish/fishfile
begin
    not functions -q fisher
    and type -q curl
    and begin
        not test -q yay
        or  test (whoami) != 'root'
    end
end
if is_succeeded
    echo "Try to install fisher..."
    set -l fisher_target $XDG_CONFIG_HOME/fish/functions/fisher.fish
    curl https://git.io/fisher --create-dirs -sLo $fisher_target
    ln -sf (__dotfiles_path)/fish/fishfile $XDG_CONFIG_HOME/fish/functions/fishfile
    fish -c fisher
    is_succeeded
        and echo "Succeed to install fisher!!"
        or  echo "Failed to install fisher"
end
#}}}1

# plugins/commands related settings {{{1
# fundle plugins settings {{{2
if functions -q fisher
    if begin functions -q bass; and type -q jq; end
        set -l tapireg_dir (__dotfiles_path)/.data/vim/plugged/tapi-reg.vim
        test -d $tapireg_dir
            and bass source $tapireg_dir/macros/vimreg.sh
    end
    if functions -q bd
        balias bc 'bd -c'
        # bdg is defined in ./functions/bdg.fish
        balias bi 'bd -i'
        balias bs 'bd -s'
    end
    if functions -q to
        function to_pick -a key
            to list | grep $key | awk '{print $3}'
        end
        if type -q fzf
            function fo
                set -l query (commandline -b)
                test -n "$query"
                    and set flags --query="$query"
                    or  set flags
                to list | fzf $flags | read select
                    and cd (printf -- $select | string replace -r '^[^\s]+ -> (.+)$' '$1')
                commandline -f repaint
            end
        end
    end
end
#}}}2
# fzf settings {{{2
if type -q fzf
    set -x FZF_DEFAULT_OPTS '--height 60% --reverse --border'
    set -x FILTER 'fzf'
    type -q rg
        and set -x FZF_DEFAULT_COMMAND 'rg --files --hidden --follow --glob "!.git/*"'
    function fd
        set -l query (commandline -b)
        test -n "$query"
            and set flags --query="$query"
            or  set flags
        fzf $flags | read select
            and cd (printf -- $select)
        commandline -f repaint
    end
end
#}}}2
# vimreg(tapi-reg.vim) settings {{{2
set -l tapireg (__dotfiles_path)/.data/vim/plugged/tapi-reg.vim
if test -d $tapireg
    source $tapireg/macros/vimreg.fish
    alias tapi 'vimreg'
end
# git-tools settings {{{2
if type -q ghq
    # run 'ghq look' with fish
    function ghq -w ghq -d "modified ghq command."
        set -l tmpshell $SHELL
        set -l ghq (which ghq)
        set SHELL (which fish)
        $ghq $argv
        set SHELL $tmpshell
    end
    if type -q fzf
        set -g GHQ_SELECTOR fzf
        set -g GHQ_SELECTOR_OPTS '--height 20% --reverse --border --color bg+:13,hl:3,pointer:7'
        function fq
            set -l query (commandline -b)
            test -n "$query"
                and set flags --query="$query"
                or  set flags
            ghq list --full-path | fzf $flags | read select
                and cd (printf -- $select)
            commandline -f repaint
        end
    end
end
#}}}2
# clipboard settings {{{2
if begin is_wsl; and type -q 'win32yank.exe'; end
    alias pbcopy  'win32yank.exe -i'
    alias pbpaste 'win32yank.exe -o'
else if type -q xsel
    alias pbcopy  'xsel -bi'
    alias pbpaste 'xsel -bo'
else if type -q termux-clipboard-set
    alias pbcopy  'termux-clipboard-set'
    alias pbpaste 'termux-clipbaord-get'
end
#}}}2
# vscode utilities {{{2
if type -q "code"
    if test "$CODE" = "1"
        function csync
            code --list-extensions > $DOTFILES/vscode/extensions.txt
        end
    else
        function cinit
            if test -n -d ~/.config/Code*
                echo "Cannot find direcrotry at ~/.config/Code*/"
                return 1
            end
            set -l root_dir "$DOTFILES/vscode"
            set -l code_dir "~/.config/Code*/User"
            ln -sf $root_dir $code_dir
            cat $root_dir/extensions.txt | xargs -L1 code --install-extension
        end
    end
end
# }}}2
# other settings {{{2
set -g DOTFILES (__dotfiles_path)
alias bool "is_succeeded; and echo true; or echo false"
if type -q nvim
    if type -q nvim-qt
        balias gnvim 'nvim-qt --no-ext-tabline'
        balias qvim  'nvim-qt --no-ext-tabline'
    end
end
if type -q su
    function fisu -w su
        su --shell=/usr/bin/fish $argv
    end
end
#}}}1

# set onedark colorscheme {{{1
if begin status is-interactive; and functions -q set_onedark; end
    set -l od_option
    if test "$TERM" = "linux"
        set_onedark_color black 000000 16
        set_onedark_color white ffffff 231
    else if set -q VIM_TERMINAL
        set od_option '-256'
    else if string match -q 'eterm-*' $TERM
        function fish_title; true; end
    end
    function onedark -w set_onedark
        test "$TERM" = "linux"
            and set_onedark_color black 000000  16
            or  set_onedark_color black default default
        if test "$CODE" = "1"
            # set base16-monokai color.
            set_onedark_color red     f92672  204
            set_onedark_color green   a6e22e  114
            set_onedark_color yellow  f4bf75 173
            set_onedark_color blue    66d9ef  75
            set_onedark_color magenta ae81ff  170
            set_onedark_color cyan    a1efe4  80
            set_onedark_color white   default 145
        else
            # set onedark color.
            set_onedark_color red     e06c75  204
            set_onedark_color green   98c379  114
            set_onedark_color yellow  default 173
            set_onedark_color blue    61afef  75
            set_onedark_color magenta c678dd  170
            set_onedark_color cyan    56b6c2  80
        end
        set_onedark_color white     default 145
        set_onedark_color brblack   default 59
        set_onedark_color brred     default default
        set_onedark_color brgreen   default default
        set_onedark_color bryellow  default 186
        set_onedark_color brblue    default 75
        set_onedark_color brmagenta default default
        set_onedark_color brcyan    default 80
        set_onedark_color brwhite   default default
        set_onedark $argv
    end
end
#}}}1

begin
    status is-interactive
    and test -f ~/.fishrc
end
if is_succeeded
    source ~/.fishrc
    alias config_local '$EDITOR ~/.fishrc'
end
