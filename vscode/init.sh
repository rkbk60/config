#!/bin/sh
HERE=$(cd $(dirname $0); pwd)
TARGET=$HOME/.config/Code/User

[ -d $TARGET ] || mkdir -P $TARGET
ln -sf $HERE/settings.json5    $TARGET/settings.json
ln -sf $HERE/keybindings.json5 $TARGET/keybindings.json
if type code 1>/dev/null 2>&1; then
    cat $HERE/extensions.txt | while read line; do
        code --install-extension $line
    done
fi
